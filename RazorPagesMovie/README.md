# RazorPagesMovie
[![build](https://img.shields.io/badge/build-passing-green.svg?logo=visual-studio-code)](https://gitee.com/trinco/ASP.NET-Core/tree/master/RazorPagesMovie)

使用 ASP.NET Core 创建 Razor 页面 Web 应用

## [开始使用ASP.NET Core中的Razor Pages](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/razor-pages-start?view=aspnetcore-2.2&tabs=visual-studio-code)
    
### 创建项目

console (CLI)
```
dotnet new webapp -o RazorPagesMovie
code -r RazorPagesMovie
```

### 信任HTTPS开发证书

console (CLI)
```
dotnet dev-certs https --trust
```
显示如下对话框

![](https://docs.microsoft.com/zh-cn/aspnet/core/getting-started/_static/cert.png?view=aspnetcore-2.2)

相关信息：[信任 ASP.NET Core HTTPS 开发证书](https://docs.microsoft.com/zh-cn/aspnet/core/security/enforcing-ssl?view=aspnetcore-2.2&tabs=visual-studio#trust-the-aspnet-core-https-development-certificate-on-windows-and-macos)

### 主要文件夹及文件
* wwwroot文件夹
        
    包含静态文件，如 HTML 文件、JavaScript 文件和 CSS 文件。

    相关信息：[ASP.NET Core 中的静态文件](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/static-files?view=aspnetcore-2.2)

* appSettings.json

    包含配置数据，如连接字符串。
        
    相关信息：[ASP.NET Core 中的配置](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/configuration/index?view=aspnetcore-2.2)

* Program.cs

    包含程序的入口点。

    相关信息：[.NET 通用主机](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-2.2)

* Startup.cs

    包含配置应用行为的代码，例如，是否需要同意 cookie。

    相关信息：[ASP.NET Core 中的应用启动](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/startup?view=aspnetcore-2.2)

## [在 ASP.NET Core 中向 Razor Pages 应用添加模型](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/model?view=aspnetcore-2.2&tabs=visual-studio)
### 添加所需的 NuGet 包
console (CLI)
```
dotnet add package Microsoft.EntityFrameworkCore.SQLite
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.Design
```

### 辅助搭建movie模型
#### 安装scaffolding tool
console (CLI)
```
dotnet tool install --global dotnet-aspnet-codegenerator
```
#### 生成
console (CLI)
```
dotnet aspnet-codegenerator razorpage -m Movie -dc RazorPagesMovieContext -udl -outDir Pages\Movies --referenceScriptLibraries
```
#### 参数说明
|Parameter|Description|
|--------|-----------|
|-m|The name of the model|
|-dc|The DbContext class to use|
|-udl|Use the default layout|
|-outDir|The relative output folder path to create the views.|
|--referenceScriptLibraries|Adds _ValidationScriptsPartial to Edit and Create pages|
|-h|Help|

相关信息：[dotnet aspnet-codegenerator](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/tools/dotnet-aspnet-codegenerator?view=aspnetcore-2.2)


### 初始化数据库迁移
console(CLI)
```
dotnet ef migrations add InitialCreate
dotnet ef database update
```
ef migrations add InitialCreate 命令生成用于创建初始数据库架构的代码。 此架构的依据为 DbContext 中指定的模型（在 RazorPagesMovieContext.cs 文件中） 。 InitialCreate 参数用于为迁移命名。 可以使用任何名称，但是按照惯例，会选择可说明迁移的名称。

ef database update 命令在 Migrations/<time-stamp>_InitialCreate.cs 文件中运行 Up 方法 。 Up 方法会创建数据库。

## [ASP.NET Core 中已搭建基架的 Razor 页面](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/page?view=aspnetcore-2.2&tabs=visual-studio-code)
[Razor 保留关键字](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/views/razor?view=aspnetcore-2.2#razor-reserved-keywords)

[Razor 语法](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/views/razor?view=aspnetcore-2.2#razor-syntax)

## [使用数据库和 ASP.NET Core](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/sql?view=aspnetcore-2.2&tabs=visual-studio)
添加数据库种子

## [在 ASP.NET Core 应用中更新生成的页面](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/da1?view=aspnetcore-2.2)
[数据类型](https://docs.microsoft.com/zh-cn/ef/core/modeling/relational/data-types)

[处理并发冲突](https://docs.microsoft.com/zh-cn/aspnet/core/data/ef-rp/concurrency?view=aspnetcore-2.2&tabs=visual-studio)

## [将搜索添加到 ASP.NET Core Razor 页面](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/search?view=aspnetcore-2.2)
[表单标记帮助程序](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/views/working-with-forms?view=aspnetcore-2.2#the-form-tag-helper)

[输入标记帮助程序](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/views/working-with-forms?view=aspnetcore-2.2#the-input-tag-helper)

## [将新字段添加到 ASP.NET Core 中的 Razor 页面](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/new-field?view=aspnetcore-2.2&tabs=visual-studio)

>备注
>
>在本教程中，使用 Entity Framework Core 迁移功能（若可行） 。 迁移会更新数据库架构，使其与数据模型中的更改相匹配。 但是，迁移仅能执行 EF Core 提供程序所支持的更改类型，且 SQLite 提供程序的功能将受限。 例如，支持添加列，但不支持删除或更改列。 如果已创建迁移以删除或更改列，则 ef migrations add 命令将成功，但 ef database update 命令会失败。 由于上述限制，本教程不对 SQLite 架构更改使用迁移。 转而在架构更改时，放弃并重新创建数据库。
>
>要绕开 SQLite 限制，可手动写入迁移代码，在表内容更改时重新生成表。 表重新生成涉及：
>
>* 创建新表。
>* 将旧表中的数据复制到新表中。
>* 放弃旧表。
>* 为新表重命名。
>
>有关更多信息，请参见以下资源：
>
>* [SQLite EF Core 数据库提供程序限制](https://docs.microsoft.com/zh-cn/ef/core/providers/sqlite/limitations)
>* [自定义迁移代码](https://docs.microsoft.com/zh-cn/ef/core/managing-schemas/migrations/#customize-migration-code)
>* [数据种子设定](https://docs.microsoft.com/zh-cn/ef/core/modeling/data-seeding)

## [将验证添加到 ASP.NET Core Razor 页面](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/razor-pages/validation?view=aspnetcore-2.2&tabs=visual-studio)
> 软件开发关键原则：[DRY](https://wikipedia.org/wiki/Don%27t_repeat_yourself)(Don't repeat yourself)

### Next: [Razor Pages 和 EF Core 入门](https://docs.microsoft.com/zh-cn/aspnet/core/data/ef-rp/intro?view=aspnetcore-2.2&tabs=visual-studio)