# ASP.NET Core
![version](https://img.shields.io/badge/platform-Windows-yellow.svg?logo=windows)
[![star](https://gitee.com/trinco/ASP.NET-Core/badge/star.svg?theme=dark)](https://gitee.com/trinco/ASP.NET-Core/stargazers)
[![fork](https://gitee.com/trinco/ASP.NET-Core/badge/fork.svg?theme=dark)](https://gitee.com/trinco/ASP.NET-Core/members)

## 介绍
实现Microsoft官网上的ASP.NET Core教程

一次提交完成一个页面的操作。

使用Visual Studio Core。

使用ASP.NET Core的.NET Core CLI开发。

基于ASP.NET Core 2.2。

## 项目
### RazorPagesMovie
使用 ASP.NET Core 创建 Razor 页面 Web 应用


