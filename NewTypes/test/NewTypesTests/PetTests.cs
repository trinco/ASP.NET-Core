using System;
using Xunit;
using Pets;

namespace NewTypesTests
{
    public class PetTests
    {
        [Fact]
        public void DogTalkToOwnerReturnsWoof()
        {
            var expected = "Woof!";
            var actual = new Dog().TalkToOwner();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CatTalkToOwnerReturnsMeow()
        {
        //Given
            var expected = "Meow!";
            var actual = new Cat().TalkToOwner();
        //When
        
        //Then
            Assert.Equal(expected, actual);
        }
    }
}
