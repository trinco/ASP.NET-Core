# 使用 .NET Core 命令行组织和测试项目
## 代码目录结构
```
/NewTypes
|__/src
   |__/NewTypes
      |__/Pets
         |__Dog.cs
         |__Cat.cs
         |__IPet.cs
      |__Program.cs
      |__NewTypes.csproj
|__/test
   |__NewTypesTests
      |__PetTests.cs
      |__NewTypesTests.csproj
```
## 创建测试
```
dotnet new xunit
```

## 添加项目引用
```
dotnet add reference ../../src/NewTypes.csproj
```

## 测试命令
```
dotnet test
```