# HelloWorld
## 初始化
```CLI
dotnet new console
```

## 解析生成资产
* 对于 .NET Core 1.x ，键入`dotnet restore`。 运行`dotnet restore`后，便有权访问生成项目所需的 .NET Core 包。
> ***备注***   
从 .NET Core 2.0 SDK 开始，无需运行 dotnet restore，因为它由所有需要还原的命令隐式运行，如 dotnet new、dotnet build 和 dotnet run。 在执行显式还原有意义的某些情况下，例如 Azure DevOps Services 中的持续集成生成中，或在需要显式控制还原发生时间的生成系统中，它仍然是有效的命令。

