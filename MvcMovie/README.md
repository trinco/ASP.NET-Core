# 使用 ASP.NET Core MVC 创建 Web 应用
## ASP.NET Core MVC 入门
### 创建应用
```CLI
dotnet new mvc -o MvcMovie
code -r MvcMovie
```

## 将控制器添加到 ASP.NET Core MVC 应用
*  MVC 所用的默认URL路由逻辑使用如下格式来确定调用的代码：
```
/[Controller]/[ActionName]/[Parameters]
```
* 使用 HtmlEncoder.Default.Encode 防止恶意输入（即 JavaScript）损害应用。
* URL模板
```C#
app.UseMvc(routes =>
{
    routes.MapRoute(
        name: "default",
        template: "{controller=Home}/{action=Index}/{id?}");
});
```

## 将视图添加到 ASP.NET Core MVC 应用
* 控制器方法（亦称为“操作方法” ，如上面的 Index 方法）通常返回 IActionResult（或派生自 ActionResult 的类）。
* 查找 @RenderBody() 行。 RenderBody 是显示创建的所有特定于视图的页面的占位符，已包装在布局页面中 。

## 将模型添加到 ASP.NET Core MVC 应用
* 安装ASP.NET Core代码生成器
```
dotnet tool install --global dotnet-aspnet-codegenerator
```
* 运行命令
```
dotnet aspnet-codegenerator controller -name MoviesController -m Movie -dc MvcMovieContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries
```
|参数|说明|
|-|-|
|-m|模型的名称。|
|-dc|数据上下文。|
|-udl|使用默认布局。|
|--relativeFolderPath|用于创建文件的相对输出文件夹路径。|
|--useDefaultLayout|应为视图使用默认布局。|
|--referenceScriptLibraries|向“编辑”和“创建”页面添加 _ValidationScriptsPartial|

* 有关详细信息，请查看[dotnet aspnet-codegenerator](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/tools/dotnet-aspnet-codegenerator?view=aspnetcore-2.2)

* 数据库迁移
```
dotnet ef migrations add InitialCreate
dotnet ef database update
```

## 在 ASP.NET Core 中使用 SQL
* [SQLite官网](https://www.sqlite.org/index.html)

## ASP.NET Core 中的控制器方法和视图
